FLAGS = $$(pkg-config --cflags glib-2.0) -W -Wall $(CFLAGS)
CC = gcc

all: conf-update

config.o: config.c conf-update.h
	$(CC) $(FLAGS) -c config.c
core.o: core.c conf-update.h
	$(CC) $(FLAGS) -c core.c
helpers.o: helpers.c conf-update.h
	$(CC) $(FLAGS) -c helpers.c
conf-update.o: conf-update.c conf-update.h
	$(CC) $(FLAGS) -c conf-update.c
index.o: index.c conf-update.h
	$(CC) $(FLAGS) -c index.c
modified.o: conf-update.h modified.c
	$(CC) $(FLAGS) -c modified.c

conf-update.h: core.h helpers.h index.h modified.h config.h

conf-update: core.o helpers.o conf-update.o index.o modified.o config.o
	$(CC) $(LDFLAGS) -o conf-update config.o core.o helpers.o conf-update.o index.o modified.o $(shell ${PKG_CONFIG} --libs glib-2.0 ncurses menu) -lcrypto

install: conf-update
	@install -d $(DESTDIR)/usr/sbin/
	@install conf-update $(DESTDIR)/usr/sbin/
	@install -d $(DESTDIR)/etc
	@install -m 644 conf-update.conf $(DESTDIR)/etc
	@install -d $(DESTDIR)/usr/share/man/man1
	@install -m 644 conf-update.1 $(DESTDIR)/usr/share/man/man1

clean:
	rm -f *.o
	rm -f conf-update	

.PHONY: all clean
