bool user_modified(char *file);
void md52hex(unsigned char *md5sum, char *hexdigest);
void calc_md5(char *file, char* hexdigest);
void md5sum_update_file(char *file, char *hexdigest);
void md5sum_update(char *file, char *hexdigest);
