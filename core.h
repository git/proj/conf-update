char *get_real_filename(const char *update);
char *get_highest_update(char **index, char *update);
bool is_last_entry(const char *entry);
bool is_valid_entry(const char *entry);
void merge(char *update, char **index);
int show_diff(char *update);
int edit_update(char *update);
char **merge_interactively(char *update, char **index);
void display_help();
