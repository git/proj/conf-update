char **get_listing(char *cmd, char *delim);
int compare_updates(const void *a, const void *b);
struct node *find_node(struct node *root, char *path);
void sanity_checks();
void draw_legend(WINDOW *inner);
void draw_background();
struct node *fold_updates(char **list);
struct node *find_node(struct node *root, char *path);
char *get_indent_name(struct node *update, int width);
void build_item_array(ITEM **item_array, struct node *root, int menu_width);
int count_array_items(struct node *root);
void free_folded(struct node *root);
bool get_confirmation(WINDOW *win, char *action);
void exit_error(bool expr, char *hint);
char **get_files_list(char *searchpath, char **index, int *max);

struct node {
	char *name;
	struct node **children;
	int ct_children;
	struct node *parent;
	bool dir;
	char **link;
};
