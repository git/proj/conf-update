#! /bin/sh
self=`basename "$0"`

if [ -z "$1" ]; then
	cat <<-EOF
		USAGE
		  ${self} <version>
		EOF
	exit 1
fi
version=$1
base="conf-update-${version}"

rm -vf "${base}".tar{,.bz2}
git archive "--prefix=${base}/" --format=tar -v "--output=${base}.tar" HEAD
bzip2 -v9 "${base}.tar"
