#define CONF_UPDATE_CONFIG_FILE "/etc/conf-update/conf-update.conf"

struct configuration {
	bool automerge_trivial;
	bool automerge_unmodified;
	bool check_actions;
	char *pager;
	char *diff_tool;
	char *merge_tool;
	char *edit_tool;
} config;

void read_config();
